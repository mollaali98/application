import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Apples {
    public static void main(String[] args) {
        int enterLineMatrix = 0;
        int enterColsMatrix = 0;

        System.out.println ( "Enter the size of the box: " );
        Scanner scanner = new Scanner ( System.in );
        String box = scanner.nextLine ();
        String[] boxToMatrix = box.split ( "x" );

        List<List<Integer>> coordinatesFinals = new ArrayList<> ();
        enterLineMatrix = Integer.parseInt ( boxToMatrix[0] );
        enterColsMatrix = Integer.parseInt ( boxToMatrix[1] );
        Integer[][] matrix = new Integer[enterColsMatrix][enterLineMatrix];

        System.out.println ( "Еnter the coordinates of the rotten apples: " );
        String coordinates = scanner.nextLine ();
        String[] coordinatesRottenApples = coordinates.split ( " " );
        for (String s : coordinatesRottenApples) {
            String[] coordinatesSingleRottenApples = s.split ( "[(,)]" );
            int x = Integer.parseInt ( coordinatesSingleRottenApples[2] );
            int y = Integer.parseInt ( coordinatesSingleRottenApples[1] );
            matrix[x][y] = 1;

            List<Integer> currentCoor = new ArrayList<> ();
            currentCoor.add ( x );
            currentCoor.add ( y );
            coordinatesFinals.add ( currentCoor );
        }

        System.out.println ( "After how many days will you come back: " );
        int days = Integer.parseInt ( scanner.nextLine () );
        int day = 3;
        int loops = days / day;

        for (List<Integer> a : coordinatesFinals) {
            int cols = a.get ( 0 );
            int rows = a.get ( 1 );
            check ( cols, rows, loops, enterColsMatrix, enterLineMatrix, matrix );
        }
        System.out.println ( "Result \n" );
        for (int i = 0; i < enterColsMatrix; i++) {
            for (int j = 0; j < enterLineMatrix; j++) {
                if (matrix[i][j] == null) {
                    System.out.print ( 0 );
                } else {
                    System.out.print ( matrix[i][j] );
                }
            }
            System.out.println ();
        }
    }

    private static void check(int applesX, int applesY, int loops, int line, int col, Integer[][] matrix) {
        int colY = applesY;
        colY--;
        int rowX = applesX;
        rowX--;
        int rowEnd = rowX + loops;
        int rowStart = rowX - loops;

        if (rowEnd > line || rowEnd == line) {
            rowEnd = line;
            rowEnd--;
        }
        if (rowStart < 0) {
            rowStart = 0;
        }
        int starCols = colY - loops;
        if (starCols < 0) {
            starCols = 0;
        }
        int cols = colY + loops;
        if (cols > col) {
            cols = col;
            cols--;
        }
        for (int j = starCols; j <= cols; j++) {
            for (int i = rowStart; i <= rowEnd; i++) {
                matrix[i][j] = 1;
            }
        }
    }
}