import java.util.Scanner;
import java.util.Stack;

public class Brackets {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
        String inputString = scanner.nextLine();
        String[] arrayOfBrackets = inputString.split("");
        Stack<String> stringStack = new Stack<>();
        boolean isCorrect = true;
        stringStack.push(arrayOfBrackets[0]);
        for (int i = 1; i < arrayOfBrackets.length; i++) {
            String currentElement = arrayOfBrackets[i];
            if (arrayOfBrackets[0].equals(")") ){
                isCorrect = false;
            }
            if (currentElement.equals("(")){
                stringStack.push(currentElement);
            }else if(currentElement.equals(")")){
                if (stringStack.isEmpty()){
                    isCorrect = false;
                    System.out.println(isCorrect);
                    return;
                }
                stringStack.pop();
            }
        }
        System.out.println(isCorrect);
    }
}

