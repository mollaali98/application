import java.util.*;

public class Sensor {
    public static void main(String[] args) {
        Scanner scanner = new Scanner ( System.in );
        List<List<String>> listOfSensors = new ArrayList<> ();
        String sensorCoordinates = scanner.nextLine ();
        while (!sensorCoordinates.equals ( "" )) {
            List<String> stringOfcoordinat = new ArrayList<> ();
            String[] a = sensorCoordinates.split ( "," );
            stringOfcoordinat.add ( a[0] );
            stringOfcoordinat.add ( a[1] );
            stringOfcoordinat.add ( a[2] );
            listOfSensors.add ( stringOfcoordinat );
            sensorCoordinates = scanner.nextLine ();
        }

        System.out.println ( "Neighbours distance: " );
        int distance = Integer.parseInt ( scanner.nextLine () );

        System.out.println ( "Max error: " );
        int error = Integer.parseInt ( scanner.nextLine () );

        List<String> result = new ArrayList<> ();

        for (int i = 0; i < listOfSensors.size (); i++) {
            List<String> currentCoordinates = listOfSensors.get ( i );
            int x = Integer.parseInt ( currentCoordinates.get ( 0 ) );
            int y = Integer.parseInt ( currentCoordinates.get ( 1 ) );
            int air = Integer.parseInt ( currentCoordinates.get ( 2 ) );
            int xPlus = x + distance;
            int xMinus = x - distance;
            int yPlus = y + distance;
            int yMinus = y - distance;
            for (int j = i + 1; j < listOfSensors.size (); j++) {
                List<String> currentCheckCoordinates = listOfSensors.get ( j );
                int xCheck = Integer.parseInt ( currentCheckCoordinates.get ( 0 ) );
                int yCheck = Integer.parseInt ( currentCheckCoordinates.get ( 1 ) );
                int airCheck = Integer.parseInt ( currentCheckCoordinates.get ( 2 ) );
                if (xPlus >= xCheck && xMinus <= xCheck && yMinus <= yCheck && yPlus >= yCheck) {
                    int check = Math.abs ( air - airCheck );
                    if (check > error) {
                        String first = "(" + x + "," + y + ")";
                        String second = "(" + xCheck + "," + yCheck + ")";
                        result.add ( first );
                        result.add ( second );
                    }
                }
            }
        }
        if (!result.isEmpty ()) {
            System.out.println ( "Please check sensors at: " );
            Set<String> set = new HashSet<> (result);
            System.out.println ( set );
        } else {
            System.out.println ( "All sensors are OK." );
        }

    }
}


